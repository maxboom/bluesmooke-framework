<?php 

class Config
{
	public $dbType;
    public $dbServer;
    public $bdUser;
    public $dbPassword;
    public $dbName;
    public $themeName;
    
    public function __construct()
    {
    	$this->_doLoadLocalConfig();
    } // end __construct
    
    private function _doLoadLocalConfig()
    {
    	$localConfigPath = URL_ROOT . "/local.php";
        if (file_exists($localConfigPath)) {
        	include_once($localConfigPath);
        }

        return true;
    } // end _doLoadLocalConfig
}