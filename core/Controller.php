<?php
require_once URL_ROOT . '/core/Utils/Utils.php';
require_once URL_ROOT . '/core/Config.php';
require_once URL_ROOT . '/core/Entity.php';
require_once URL_ROOT . '/core/Utils/Response.php';
require_once URL_ROOT . '/core/User.php';
require_once URL_ROOT . '/core/Exceptions/ExceptionListener.php';

class Controller extends Entity
{
	public $config;
	public $masters;
    public $user;
	
	public function inicialize()
	{
        set_error_handler(array(ExceptionListener::getInstance(), 'onException'));
	    parent::inicialize();
        
        $this->_prepareSystem();
	    $this->_start();
	}
	
	private function _start()
	{
        try {
            //throw new Exception("asdasdasd");
            $this->masters->PluginsMaster->doCallPlugin();
        } catch (Exception $exception) {
            $exceptionListener = ExceptionListener::getInstance();
            $exceptionListener->onExceptionByException(
                $exception
            );
        }
        
        $this->masters->GuiMaster->onDisplay();
        
        //$guiMaster = $this->controller->masters->GuiMaster;
        //echo json_encode($guiMaster->getResponse($response));
        
		return true;
	} // end start_start
	
	private function _onLoadMasters()
	{
		$masters = $this->_getMasters();
        $this->masters = new stdClass();
        
	    foreach ($masters as $masterPath) {
	        $explodePath = explode("/", $masterPath);
	        $masterName = end($explodePath);
	        require_once URL_ROOT . '/core/Masters/' 
	            . $masterName . '/'
	            . $masterName . ".php";
			
	        $masterInstance = $masterName::getInstance();
	        $masterInstance->inicialize();
	        
	        $this->masters->$masterName = $masterInstance;
		}
		
	    return true;
	} // end _onLoadMasters
    
    private function _prepareSystem()
    {
        $exceptionListener = ExceptionListener::getInstance();
        $exceptionListener->inicialize();
        $this->config = new Config();
        $this->user = User::getInstance();
        $this->user->inicialize();
        $this->_onLoadMasters();
        
        return true;
    } // end _prepareSystem
    
	private function _getMasters()
	{
	    $masterPattern = URL_ROOT . "/core/Masters/*";
	    return array_filter(glob($masterPattern), 'is_dir');
	} // end _getMasters
}