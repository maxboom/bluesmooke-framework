<?php

class ExceptionListener extends Entity
{
    private $_severity;
    
    public function onExceptionByException(Exception $exception)
    {
        return $this->onException(
          E_ERROR,
          $exception->getMessage(),
          $exception->getFile(),
          $exception->getLine()
        );
    }
    
    public function onException($errno, $errstr, $errfile, $errline)
    {
        $guiMaster = $this->controller->masters->GuiMaster;
        
        $exception = new ErrorException($errstr, 0, $errno, $errfile, $errline); 
        
        $exceptionMessage = $exception->getMessage();
        $exceptionSeverity = $exception->getSeverity();
        
        if (!$this->_hasNotIgnore($exceptionSeverity)) {
            return true;
        }

        $response = $guiMaster->getResponse();
        
        $response->setException(
            $exceptionMessage,
            $exceptionSeverity,
            $errfile,
            $errline
        );
        
        $guiMaster->setResponse($response);
        
        return true;
    } // end onException
    
    private function _hasNotIgnore($severity)
    {
        return ($severity & $this->_severity) != 0;
    } // end _hasNotIgnore
    
    public function inicialize()
    {
        parent::inicialize();
        
        $this->_severity = 
            1 * E_ERROR | 
            1 * E_WARNING | 
            0 * E_PARSE | 
            0 * E_NOTICE | 
            0 * E_CORE_ERROR | 
            0 * E_CORE_WARNING | 
            0 * E_COMPILE_ERROR | 
            0 * E_COMPILE_WARNING | 
            0 * E_USER_ERROR | 
            0 * E_USER_WARNING | 
            0 * E_USER_NOTICE | 
            0 * E_STRICT | 
            0 * E_RECOVERABLE_ERROR | 
            0 * E_DEPRECATED | 
            0 * E_USER_DEPRECATED; 
    } // end inicialize
}