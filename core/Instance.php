<?php 

class Instance
{
	private static $_instances = array();
	
	private function __construct()
	{
	} // end __construct
	
	protected function __clone()
	{
	} // end __clone
	
	public static function &getInstance()
	{
		$class = get_called_class();
		if (!isset(self::$_instances[$class])) {
            self::$_instances[$class] = new $class();
        }
        return self::$_instances[$class];
	} // end getInstance
}