<?php 

class SqlObject implements ISqlTool
{
	public function query($sql, $params = array())
	{
        $dbMaster = $this->_getDatabaseMasterInstance();
        
		return $dbMaster->query($sql, $params);
	} // end query
	
	public function insert($table, $values = array())
	{
		$dbMaster = $this->_getDatabaseMasterInstance();
                
		return $dbMaster->insert($table, $values);
	} // end insert
	
	public function remove($table, $sqlCondition = array())
	{
		$dbMaster = $this->_getDatabaseMasterInstance();
                
		return $dbMaster->remove($sql, $sqlCondition);
	} // end remove
	
	public function update($table, $values = array() , $sqlCondition = array())
	{
	    $dbMaster = $this->_getDatabaseMasterInstance();
                
		return $dbMaster->update($sql, $values, $sqlCondition);	
	} // end update
	
	public function quote($value)
	{
		$dbMaster = $this->_getDatabaseMasterInstance();
                
		return $dbMaster->quote($value);
	} // end quote
	
	public function massInsert($table, $values = array())
	{
		$dbMaster = $this->_getDatabaseMasterInstance();
                
		return $dbMaster->massInsert($table, $values);
	} // end massInsert
    
    private function _getDatabaseMasterInstance()
    {
        $controller = Controller::getInstance();
        return $controller->masters->DatabaseMaster;
    }
}