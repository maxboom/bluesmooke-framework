<?php 

class GuiMaster extends Entity
{
	private $_properties;
	
	public function inicialize()
	{
		parent::inicialize();
		$this->_setDefaultProperties();
		
		return true;
	} // end inicialize
        
	public function onDisplay()
	{
        if ($this->_hasRequestMethodPost()) {
            return $this->displayResponse();
        }
        
		$themePath = $this->_properties['theme_path'];

		if (!file_exists($themePath)) {
			$errMsg = __("Theme %s is not found", $themePath);
			throw new Exception($errMsg);
		}
		
		$indexPath = $themePath . "index.phtml";
		
		if (!file_exists($indexPath)) {
			$errMsg = __("index.php not found from %s theme", $themePath);
			throw new Exception($errMsg);
		}
		
		include $indexPath;
		
		return true;
	} // end onDisplay
    
    public function displayResponse()
    {
        $response = $this->getResponse();
        $options = $response->_options;
        echo json_encode($options);
        return true;
    } // end displayResponse
	
    public function fetchSystemHead()
    {
        $systemHeadPath = "/core/Masters/GuiMaster/template/head.phtml";
        
        ob_start();
        include_once $systemHeadPath;
        return ob_get_clean();
    } // end fetchSystemHead
    
    public function fetchSystemFooter()
    {
        $systemFooterPath = "core/Masters/GuiMaster/template/footer.phtml";
        
        ob_start();
        include_once $systemHeadPath;
        return ob_get_clean();
    } // end fetchSystemFooter
    
	public function fetch($fileName)
	{
		$themePath = $this->_properties['theme_path'];
		$filePath = $themePath . $fileName;
		
		if (!file_exists($filePath)) {
			$errMsg = __("File %s is not exsist!", $filePath);
			throw new Exception($errMsg);
		}
		
		ob_start();
		include $filePath;
		return ob_get_clean();
	}
	
	public function setTitle($caption)
	{
		$this->_properties['caption'] = $caption;
		
		return true;
	} // end setTitle
	
	public function addCss($cssFile)
	{		
		if (!file_exists($cssFile)) {
			$errMsg = __("Css file %s is not exist!", $cssFile);
			throw new Exception($errMsg);
		}
		
		$this->_properties['css'][] = $cssFile;
		return true;
	} // end addCss
	
	public function addJs($jsFile)
	{
		if (!file_exists($jsFile)) {
			$errMsg = __("Js file %s is not exist!", $jsFile);
			throw new Exception($errMsg);
		}

	    $this->_properties['js'][] = $jsFile;
	    return true;
	} // end addJs
	
	public function addMeta($metaName, $metaValue)
	{
		$this->_properties['meta'][$metaName] = $metaValue;
		return true;
	} // end addMeta
	
    public function getResponse()
    {
        return $this->getProperty("response");
    } // end getResponce
    
	public function &getProperty($property)
	{
		if (!array_key_exists($property, $this->_properties)) {
			$errMsg = __("Property %s not exist!", $property);
			throw new Exception($errMsg);
		}
		
		return $this->_properties[$property];
	} // end getProperty
    
    public function setProperty($propertyName, $value)
    {
        $this->_properties[$propertyName] = $value;
        
        return true;
    } // setProperty
	
    public function setResponse(Response $response)
    {
        $this->_properties['response'] = $response;
        
        return true;
    } // end setResponse
    
    public function setIcon($icon)
    {
        $this->_properties['icon'] = $icon;
        
        return true;
    } // end setIcon
    
    public function setTheme($themeName)
    {
        $themePath = URL_ROOT . "/themes/" . $themeName . "/";
        $this->setProperty("theme", $themeName);
        $this->setProperty("theme_path", $themePath);
        
        return true;
    } // end setTheme
    
    public function getThemeName()
    {
        return $this->getProperty("theme");
    } // end getThemeName
    
    public function getThemePath()
    {
        return $this->getProperty("theme_path");
    } // end getThemePath
	
	private function _setDefaultProperties()
	{
        $themeName = $this->controller->config->themeName;
        $this->setTheme($themeName);
        
        $themePath = $this->getThemePath();
        $startPath = $themePath . 'start.php';
        
        if (!file_exists($startPath)) {
            $startPath = "/core/masters/GuiMaster/template/start.php";
        }
        
        include_once $startPath;

        $this->setTheme($themeName);
        $this->setResponse(new Response());
		return true;
	} // end _setDefaultProperties
    
    private function _hasRequestMethodPost()
    {
        return $_SERVER['REQUEST_METHOD'] == "POST";
    } // end _hasRequestMethodPost
}