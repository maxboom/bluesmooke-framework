/* global response */

var callback_action = null;

function doResponse(response) {
    console.log(response);
    
    if (callback_action) {
        callback_action(response);
        return;
    }

    switch (response.action) {
        case 'redirect': {
            onRedirectAction(response);
        break;
        }
        case 'message': {
            onMessageAction(response);
        break;
        }
        case 'ajax': {
            onAjaxAction(response);
        break;
        }
        case 'reload': {
            onReloadAction();
        break;
        }
        case 'exception': {
            onExceptionAction(response);
        break;
        }
    }
    //console.debug(response);
} // end doResponse

function setActionCallback(callback) {
    callback_action = callback;
} // end setActionCallback

function onRedirectAction(response) {
    location.href = response.url;
} // end doRedirectAction

function onReloadAction() {
    location.reload();
} // end doReloadAction

function onMessageAction(response) {
    alert(response.message);
} // end doMessageAction

function onAjaxAction(response) {
    $("#content").html(response.content);
} // end doAjaxAction

function onExceptionAction(response) {
    var append = "";
    if (response.file) {
        append += " FILE: " + response.file;
        append += " LINE: " + response.line;
    }
    alert(response.severity + ": " + response.message + append);
} // end onExceptionAction

function addIFrame() {
    $('<iframe name="gate" style="display:none;"></iframe>').appendTo('body');
} // end addIFrame

$(document).ready(function() {
    doResponse(response);
    addIFrame();
    
    $("[name='gate']").on('load', function() {
       var response = JSON.parse($(this).contents().text()); 
       console.debug(response);
       doResponse(response);
    });
});