<?php

$this->_properties = array(
    'js'         => array(
        "//code.jquery.com/jquery-1.12.0.min.js",
        "/core/Masters/GuiMaster/template/scripts/bluesmoke.js"
    ),
	'css'        => array(),
    'lang'       => "en-US",
	'caption'    => 'BlueSmoke Framework',
	'meta'       => array(
        'description' => "Free Web Framework",
        'author'      => "zmaxboomz@gmail.com",
        'keywords'    => "php, framework, js, web" 
     ),
    'icon'       => '/core/Masters/GuiMaster/template/bluesmoke.ico',
);