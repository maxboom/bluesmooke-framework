<?php 

class PluginObject extends Entity
{
	public $plugins;
	
	public function inicialize()
	{
		parent::inicialize();
		$this->_doLoadPlugins();
		$this->_doLoadDatabaseObject();
        
		return true;
	} // end inicialize
	
	private function _doLoadPlugins()
	{
        $this->plugins = new stdClass();
		$plugins = $this->_getPlugins();
		 
		foreach ($plugins as $pluginPath) {
			$pluginName = $this->_getPluginName();
	
			require_once URL_ROOT . "\\plugins\\"
				. $pluginName . "\\"
				. $pluginName . ".php";
	
			if ($this->_hasThisPlugin($pluginName)) {
				continue;
			}
				
			$this->_plugins->$pluginName = $pluginName::getInstance();
			$this->_plugins->$pluginName->inicialize();
		}
		
		return true;
	} // end _doLoadPlugins
    
    private function _doLoadDatabaseObject()
    {        
        $pluginName = $this->_getPluginName();
        $objectClassName = $pluginName . "Object";
        $objectPath = dirname($this->_getCurrentPath()) .
            "\\" . $objectClassName . ".php";
        
        if (!file_exists($objectPath)) {
            return false;
        }
        
        require_once $objectPath;
        $this->object = new $objectClassName();
        
        return true;
        
    } // end _doLoadDatabaseObject
    
    private function _getPluginName()
    {
        $currentPath = $this->_getCurrentPath();
        $currnetDirectory = dirname($currentPath);
        
        return basename($currentPath, ".php");
    }
    
    private function _getCurrentPath()
    {
        $refactor = new ReflectionClass($this);
        return $refactor->getFileName();
    }
	
	private function _getPlugins()
	{
		$pluginPattern = URL_ROOT . "/plugins/*";
		return array_filter(glob($pluginPattern), 'is_dir');
	} // end _getPlugins
	
	private function _hasThisPlugin($pluginName)
	{
		return $pluginName == __CLASS__;
	} // end _hasThisPlugin
}