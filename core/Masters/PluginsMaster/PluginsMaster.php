<?php 
require_once URL_ROOT . '/core/Masters/PluginsMaster/PluginObject.php';
require_once URL_ROOT . '/core/Masters/PluginsMaster/Plugin.php';

class PluginsMaster extends PluginObject
{
	public function inicialize()
	{
		parent::inicialize();
		
		return true;
	} // end inicialize
	
	public function doCallPlugin()
	{
		return $this->_doInitPlugin();
	} // end doCallPlugin
	
	public function &getPluginInstance($pluginName)
	{
		$this->_plugins->$pluginName;
	} // end getPluginInstance
	
	private function _doInitPlugin()
	{
		$this->_onInitRedirector();
		return $this->plugins->router->onInit();
		//get called plugin and call onInit method him
	} // end _doInitPlugin
	
	private function _onInitRedirector()
	{
		require_once URL_ROOT . "/core/Masters/PluginsMaster/Router/Router.php";
		    		
		$this->plugins->router = Router::getInstance();
	    $this->plugins->router->inicialize();
        
        return true;
	} // end _onInitRedirector
}