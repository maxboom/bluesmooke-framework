<?php 

class RouterObject extends SqlObject
{
	private $_tableName = "bluesmoke_queries";
    
    public function getByQuery($query)
    {
        $sql = "SELECT queries.id, queries.ident_plugin, queries.regular, queries.method, plugins.caption , group_concat(access.ident_access_group separator ',') as groups
            FROM " . $this->_tableName . " queries
            LEFT JOIN `bluesmoke_queries2access_group` access ON queries.id = access.id_query
            LEFT JOIN `bluesmoke_plugins` plugins ON queries.ident_plugin = plugins.ident
            WHERE " . $this->quote($query) . " REGEXP queries.regular";
            
        return $this->query($sql);
    } // end getByQuery
    
    public function getAll()
    {
        $sql = $this->_getSql() . 
            "LEFT JOIN bluesmoke_plugins ON" .
            "(" . $this->_tableName . ".ident_plugin = bluesmoke_plugins.ident)";
            
        return $this->query($sql);
    } // end getAll
    
	private function _getSql()
	{
		$sql = "SELECT " .
				    "* " .
	           "FROM " . $this->_tableName . " ";
               
        return $sql;
	} // end _getSql
}