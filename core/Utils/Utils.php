<?php 

function __($text, $string = null)
{
	if (!is_null($string)) {
	    return sprintf($text, $string);
	}
	
	return $text;
}